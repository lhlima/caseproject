resource "aws_security_group" "autoscaling" {
  name_prefix = var.name_app
  description = "Security group that allows ssh/http and all egress traffic"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 80
    protocol  = "tcp"
    to_port   = 80
  }

  ingress {
    from_port = 443
    protocol  = "tcp"
    to_port   = 443
  }
  egress {
    from_port   = 0
    protocol    = -1
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name       = "ASG-${var.name_app}"
    Automation = "Terraform"
  }
}

resource "aws_launch_configuration" "this" {
  name_prefix                 = var.name_app
  image_id                    = var.ami
  instance_type               = var.instance_type
  key_name                    = var.key_pair
  security_groups             = [aws_security_group.autoscaling.id]
  associate_public_ip_address = true
  user_data                   = file("ec2_setup.sh")
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "this" {
  name_prefix               = var.name_app
  vpc_zone_identifier       = [aws_subnet.public_a.id, aws_subnet.public_b.id]
  launch_configuration      = aws_launch_configuration.this.name
  max_size                  = 5
  min_size                  = 2
  health_check_grace_period = 300
  health_check_type         = "ELB"
  force_delete              = true
  target_group_arns         = [aws_lb_target_group.tg.arn]
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_policy" "scalup" {
  name                   = "Scale Up"
  autoscaling_group_name = aws_autoscaling_group.this.name
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = "1"
  cooldown               = "300"
  policy_type            = "SimpleScaling"
}

resource "aws_autoscaling_policy" "scaldown" {
  name                   = "Scale Down"
  autoscaling_group_name = aws_autoscaling_group.this.name
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = "-1"
  cooldown               = "300"
  policy_type            = "SimpleScaling"
}