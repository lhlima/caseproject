provider "aws" {
  region = var.region
  access_key = var.access_key
  secret_key = var.secret_key
}

#terraform {
#  backend "s3" {
#    region = var.region
#    bucket = "sre-devops"
#    key = providers/aws/dev/var.region/var.name_app/terraform-tfstate
#    encrypt = true
#  }
#}