resource "aws_route_table" "route-public" {
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
  tags = {
    Name       = var.name_app
    Automation = "Terraform"
  }
}

resource "aws_route_table_association" "public_a" {
  route_table_id = aws_route_table.route-public.id
  subnet_id      = aws_subnet.public_a.id
}

resource "aws_route_table_association" "public_b" {
  route_table_id = aws_route_table.route-public.id
  subnet_id      = aws_subnet.public_b.id
}