resource "aws_security_group" "sg-app" {
  description = "Allow public inboud traffic"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
    cidr_blocks = ["${var.cidr_block}"]
  }

  ingress {
    from_port   = 443
    protocol    = "tcp"
    to_port     = 443
    cidr_blocks = ["${var.cidr_block}"]
  }

  #Liberar somente as conexões atraves da rede VPN
  #Essa regra está liberada somente para efeito de app de teste
  ingress {
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
    cidr_blocks = ["${var.cidr_block}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["${var.cidr_block}"]
  }
  tags = {
    Name       = "${var.name_app}-SG"
    Automation = "Terraform"
  }
}