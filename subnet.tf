resource "aws_subnet" "public_a" {
  cidr_block        = "172.24.1.0/24"
  vpc_id            = aws_vpc.vpc.id
  availability_zone = "${var.region}a"
  tags = {
    Name       = "${var.name_app}a"
    Automation = "Terraform"
  }
}

resource "aws_subnet" "public_b" {
  cidr_block        = "172.24.2.0/24"
  vpc_id            = aws_vpc.vpc.id
  availability_zone = "${var.region}b"
  tags = {
    Name       = "${var.name_app}b"
    Automation = "Terraform"
  }
}