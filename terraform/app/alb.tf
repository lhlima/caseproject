resource "aws_security_group" "alb" {
  description = "App Load Balancer SG"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    protocol    = "tcp"
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    protocol    = -1
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name       = "SG-${var.name_app}"
    Automation = "Terraform"
  }
}

resource "aws_lb" "lb" {
  name            = var.name_app
  security_groups = [aws_security_group.alb.id]
  subnets = [aws_subnet.public_a.id,
  aws_subnet.public_b.id]
  tags = {
    Name       = "LB-${var.name_app}"
    Automation = "Terraform"
  }
}

resource "aws_lb_target_group" "tg" {
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.vpc.id

  health_check {
    path              = "/"
    healthy_threshold = 2
  }

  tags = {
    Name       = "TG-${var.name_app}"
    Automation = "Terraform"
  }
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.lb.arn
  port              = 80
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tg.arn
  }
}

#resource "aws_lb_listener" "https" {
#  load_balancer_arn = aws_lb.lb.arn
#  port              = 443
#  protocol          = "HTTPS"
#  default_action {
#    type             = "forward"
#    target_group_arn = aws_lb_target_group.tg.arn
#  }
#}