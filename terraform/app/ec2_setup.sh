#!/usr/bin/bash
sudo apt-get update -y
sudo apt-get install apache2 -y

echo "Teste App" > /var/www/html/index.html
sudo ufw allow 'Apache Full'
sudo systemctl status apache2
service apache2 start

sudo apt-get install epel-release -y
sudo apt-get install stress -y