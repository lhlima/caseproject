variable "cidr_block" {
  default = "0.0.0.0/0"
}

variable "region" {
  default = "us-east-2"
}

variable "name_app" {
  default = "SG-App"
}

variable "cidr_block_vpc" {
  default = "172.24.0.0/16"
}

variable "ami" {
  default = "ami-0d5d9d301c853a04a"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "key_pair" {
  default = "DesktopCentral-KeyPair"
}