resource "aws_vpc" "vpc" {
  cidr_block = var.cidr_block_vpc
  tags = {
    Name       = var.name_app
    Automation = "Terraform"
  }
}